# Denford CNC GRBL Controller

A drop in replacement for Denford and ScanTec MicroMill and MicroTurn controllers. It features input protection and breaks out useful pins like probing and cycle start. It can be controlled through a computer over a USB or bluetooth, and from a phone or tablet using a USB-OTG adaptor or Bluetooth. There are a number of free programs and apps to choose from.

It was designed for the SmartStep board but we are exploring compatibility with the NextMove line of boards.

<img src="doc/images/controller_r.jpg" width="500">

## Build one
To see what components you need and where to place them check out the [interactive BOM](https://damped.gitlab.io/denfordcnc/ibom.html) generated using [this plugin](https://github.com/openscopeproject/InteractiveHtmlBom)

<img src="doc/images/render.png"  width="500">


The latest [schematic](https://damped.gitlab.io/denfordcnc/schematic.pdf) will be automatically updated. [Older schematics](https://gitlab.com/damped/denfordcnc/-/tree/master/doc/projectSchematics) can be found in the doc folder.

### Connections
I used the same connections that my machine came with when I designed the GRBL controller board, but in case your machine is different here is the pinout.

#### Homing Switches

All switches are normal closed to ground and pulled up to 5V on the GRBL controller board.

|Axis|Smartstep Connection|NextMove?|
|---|---|---|
|X|Home-0|DIN12|
|Y|Home-1|DIN13|
|Z|Home-2|DIN14|

#### Inputs
|Name|Smartstep Connection|NextMove?|Comment|
|---|---|---|---|
|Probe|User in 5|DIN5|Make sure the tool is ground and the probing pad is input 5|
|Door|User in 6|DIN6|I usually disable the door with a magnet and use safety glasses|
|Start/Resume|User in 7|DIN7|I start/stop through software|
|STOP|STOP|DIN11|My machine cuts all power when e-stop is triggered|

#### Outputs
|Name|Smartstep Connection|NextMove?|Comment|
|---|---|---|---|
|Spindle enable|User out 0|DOUT0|Some machines come pre wired with a relay that swiches the power to the spindle speed controller|
|Spindle Dir|User out 4|DOUT4|I dont think the speed controllers with theese machines can do this|
|Spindle PWM|User out 5|DOUT5|Spindle Speed or Laser Mode (5v PWM signal). Not interfaced to the onboard spindle speed controller (if you have one) but can be done|
|Coolant|User out 7|DOUT7| |
|USR-OUT-COM|N/A|OUT COM0|Common diode clamp. Connected to the high side of the relay coil to prevent inductive spikes (search "flyback diode" for explination)|


### Machine Compatibility
[Machine Compatibility](https://gitlab.com/damped/denfordcnc/-/blob/master/doc/machineCompatibility/machineCompatibility.md)


### Bluetooth
The board has a 4 pin header in the top right corner to plug in a Bluetooth modul. The module is from JY-MCU (BT BOARD V1.2) HC-06 (only slave) but I think a HC-05 should work too. I had to configure the baud rate to match that of the Arduino Nano to 115200 using AT commands (I'll add the steps if anyone is interested).


<img src="doc/images/bluetooth_r.jpg" width="500">


### GRBL Configuration

Once you have flashed GRBL to the microcontroller you can change the settings by connecting to it and sending


$$      to View current settings


$x=val  to Write GRBL setting

<details>
<summary>Click to expand configuration settings</summary>

|$x=val | Setting Description, Units|
|--- | ---|
|$0=10 | Step pulse time, microseconds|
|$1=25 | Step idle delay, milliseconds|
|$2=0 | Step pulse invert, mask|
|$3=0 | Step direction invert, mask|
|$4=0 | Invert step enable pin, boolean|
|$5=1 | Invert limit pins,boolean|
|$6=0 | Invert probe pin, boolean|
|$10=2 | Status report options, mask|
|$11=0.010 | Junction deviation, millimeters|
|$12=0.002 | Arc tolerance, millimeters|
|$13=0 | Report in inches, boolean|
|$20=1 | Soft limits enable, boolean|
|$21=1 | Hard limits enable, boolean|
|$22=1 | Homing cycle enable, boolean|
|$23=0 | Homing direction invert, mask|
|$24=25.000 | Homing locate feed rate, mm/min|
|$25=500.000 | Homing search seek rate, mm/min|
|$26=250 | Homing switch debounce delay, milliseconds|
|$27=2.000 | Homing switch pull-off distance, millimeters|
|$30=1000 | Maximum spindle speed, RPM|
|$31=0 | Minimum spindle speed, RPM|
|$32=0 | Laser-mode enable, boolean|
|$100=314.961 | X-axis steps per millimeter|
|$101=314.961 | Y-axis steps per millimeter|
|$102=314.961 | Z-axis steps per millimeter|
|$110=500.000 | X-axis maximum rate, mm/min|
|$111=500.000 | Y-axis maximum rate, mm/min|
|$112=500.000 | Z-axis maximum rate, mm/min|
|$120=10.000 | X-axis acceleration, mm/sec^2|
|$121=10.000 | Y-axis acceleration, mm/sec^2|
|$122=10.000 | Z-axis acceleration, mm/sec^2|
|$130=205.000 | X-axis maximum travel, millimeters|
|$131=65.000 | Y-axis maximum travel, millimeters|
|$132=121.000 | Z-axis maximum travel, millimeters|

</details>

## Buy one
I ordered some PCBs and the design works great. I have some extra ones [for sale on tindie](https://www.tindie.com/products/damped/denford-micro-mill-cnc-grbl-control-board-v002/) if you are interested.

### Contact
Contact me though my [website](http://damped.ca) or open an issue or merge request.

